package com.info.basic.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bianyadong
 * @version HelloController, v 0.1 bianyadong
 * @date 2018/3/22 16:36
 * @comment
 */
@RestController
public class HelloController {

    @RequestMapping(value = "/hello")
    private String hello(){
        return "hello 你好";
    }
}
