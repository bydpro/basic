package wap;

import com.info.basic.HelloTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author bianyadong
 * @version HelloWorld, v 0.1 bianyadong
 * @date 2018/3/23 11:07
 * @comment
 */
@Component
public class HelloWorld {

    @Autowired
    private HelloTest helloTest;


    private void string(){
        System.out.println(helloTest.toString());
    }
}
