package com.info.basic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author bianyadong
 * @version HelloController, v 0.1 bianyadong
 * @date 2018/3/22 16:36
 * @comment
 */
@RestController
public class HelloController {

    @Autowired
    private HelloService helloService;

    @Value("${from}")
    private String msg;

    @RequestMapping(value = "feign-consumer")
    private String hello(){
        return helloService.hello();
    }
}
