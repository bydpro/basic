package com.info.basic.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author bianyadong
 * @version HelloService, v 0.1 bianyadong
 * @date 2018/3/22 16:35
 * @comment
 */
@FeignClient("hello-service")
public interface HelloService {

    @RequestMapping("/hello")
    String hello();
}
