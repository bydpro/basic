package com.info.basic;

import org.springframework.stereotype.Component;

/**
 * @author bianyadong
 * @version HelloTest, v 0.1 bianyadong
 * @date 2018/3/23 11:23
 * @comment
 */
@Component
public class HelloTest {

    @Override
    public String toString() {
        return "你好";
    }
}
